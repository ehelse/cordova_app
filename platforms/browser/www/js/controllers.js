﻿angular.module('fhirApp.controllers', []).controller('ObsController', ['$scope', '$q', 'BLE', 'Settings', function ($scope, $q, BLE, Settings) {
			
			// Init
            $scope.settings = Settings.getSettings();
            $scope.BLE = BLE;
            $scope.sensorValues = BLE.getSensorValues();
            $scope.sensorMessage = BLE.getSensorMessage();
            $scope.state = Math.round(Math.random() * 100000000).toString();

            document.addEventListener("deviceready", onDeviceReady, false);
			
            function onDeviceReady() {
                $scope.scan();
            }

            $scope.getPatientInfo = function (defer) {
                var url = $scope.settings.FHIR_URL + "/Patient/" + $scope.settings.patientId;
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
					contentType: 'application/json+fhir;charset=utf-8',
					headers: {
						"Authorization":"Bearer " + $scope.accessToken
					}
                })
                    .done(function (pt) {
                        $scope.$apply(function () {
                            $scope.patient = pt;
                            // console.log('patient = ' + JSON.stringify($scope.patient));
                            var name = pt.name[0].given.join(" ") + " " + pt.name[0].family.join(" ");
                            $scope.patientName = name;
                            // console.log('patient = ' + $scope.patientName);
                            $scope.loggedIn = true;
                            defer.resolve();
                        });
                    })
                    .fail(function (error) {
                        console.log('Error getting patient:' + error + ',' + JSON.stringify(error));
                        defer.reject(-1);
                    });
            };

            $scope.saveHRM = function (pulse) {
                $scope.message = 'Saving data';

                var jsonPulse = {
                    "resourceType": "Observation",
                    "status": "final",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">'+ pulse + ' bpm heart rate</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "149530",
                                "display": "MDC_PULS_OXIM_PULS_RATE"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": pulse,
                        "unit": "bpm",
                        "system": "urn:std:iso:11073:10101",
                        "code": "264864"
                    },
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.settings.patientId}
                };
                $scope.submitObservation(jsonPulse);
            };

            $scope.savePulseOximeterData = function (pId, Sp02, pulse) {
                $scope.message = 'Saving data';
                var jsonO2 = {
                    "resourceType": "Observation",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, ' + pulse + ' bpm heart rate</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "150456",
                                "display": "MDC_PULS_OXIM_SAT_O2"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": Sp02,
                        "unit": "percent",
                        "system": "urn:std:iso:11073:10101",
                        "code": "262688"
                    },
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.settings.patientId}
                };

                var jsonPulse = {
                    "resourceType": "Observation",
                    "status": "final",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, ' + pulse + ' bpm heart rate</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "149530",
                                "display": "MDC_PULS_OXIM_PULS_RATE"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": pulse,
                        "unit": "bpm",
                        "system": "urn:std:iso:11073:10101",
                        "code": "264864"
                    },
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.settings.patientId}
                };				
                $scope.submitObservation(jsonO2);
                $scope.submitObservation(jsonPulse);
            };			
			
            $scope.submitObservation = function (json) {     
				var accessToken = decodeURIComponent(sessionStorage.getItem('fhirAccessToken'));				
				
                $.ajax({
                    type: "POST",
                    url: $scope.settings.FHIR_URL + '/Observation',
                    headers: {
                        "Authorization":"Bearer " + accessToken
                    },
                    data: JSON.stringify(json),
                    success: function (data) {
                        console.log('From server:' + JSON.stringify(data));
                        $scope.$apply(function () {
                            $scope.message = 'Data saved to cloud. '+JSON.stringify(data.issue[0].diagnostics);
                        });
                    },
                    datatype: 'json',
                    contentType: 'application/json+fhir;charset=utf-8'
                }).fail(function (error) {
                    console.log('Save error:' + error + ',' + JSON.stringify(error));
                }).done(function (data) {
                    console.log("Returned success from server. Sample of data:" + JSON.stringify(data));
                });
            };			

            $scope.submitBundle = function (json) {
                $.ajax({
                    type: "POST",
                    url: $scope.settings.FHIR_URL + '/Bundle',
                    headers: {
                        "Authorization":"Bearer "+$scope.accessToken
                    },
                    data: JSON.stringify(json),
                    success: function (data) {
                        console.log('From server:' + JSON.stringify(data));
                        $scope.$apply(function () {
                            $scope.message = 'Data saved to cloud';
                        });
                    },
                    datatype: 'json',
                    contentType: 'application/json+fhir;charset=utf-8'
                }).fail(function (error) {
                    console.log('Save error:' + error + ',' + JSON.stringify(error));
                }).done(function (data) {
                    console.log("Sample of data:" + JSON.stringify(data));
                });
            };

            $scope.saveBP = function (pId, sys, dia) {
                $scope.message = 'Saving data';

                var jsonSys =
                {
                    "resourceType": "Observation",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\"> Systolic:' + sys + '</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "code": "150020",
                                "display": "MDC_PRESS_BLD_NONINV"
                            }
                        ]
                    },
                    "component": [
                        {
                            "code": {
                                "coding": [
                                    {
                                        "code": "150021",
                                        "display": "MDC_PRESS_BLD_NONINV_SYS"
                                    }
                                ]
                            },
                            "valueQuantity": {
                                "value": sys,
                                "unit": "MDC_DIM_MMHG",
                                "code": "266016"
                            }
                        },
                        {
                            "code": {
                                "coding": [
                                    {
                                        "code": "150022",
                                        "display": "MDC_PRESS_BLD_NONINV_DIA"
                                    }
                                ]
                            },
                            "valueQuantity": {
                                "value": dia,
                                "unit": "MDC_DIM_MMHG",
                                "code": "266016"
                            }
                        }
                    ],
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.settings.patientId}
                };
                $scope.submitObservation(jsonSys);
            };

            $scope.saveWeight = function (pId, weight) {
                $scope.message = 'Saving data';

                var json = {
                    "resourceType": "Observation",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + weight + ' kg</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "188736",
                                "display": "MDC_MASS_BODY_ACTUAL"
                            }
                        ]
                    },
                    "effectiveDateTime": new Date(),
                    "valueQuantity": {
                        "value": weight,
                        "unit": "kg",
                        "system": "urn:std:iso:11073:10101",
                        "code": "263875"
                    },
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.settings.patientId}
                };
                $scope.submitObservation(json);
            };

            $scope.saveSettings = function () {
                Settings.saveSettings();
            };
			
			$scope.loginInAppBrowser = function() {			
				var ref = cordova.InAppBrowser.open($scope.settings.authUrl, '_blank', 'location=yes');
				ref.addEventListener('loadstart', function(event) { console.log(event.url);
					if (event.url.indexOf('access_token=') != -1) {
						ref.close();
						var accessToken = event.url.substring(event.url.indexOf('access_token=')+13);
						accessToken = decodeURIComponent(accessToken.substr(0,accessToken.indexOf('&')));
						console.log('Received Access Token: ' + accessToken);
						sessionStorage.setItem('fhirAccessToken', accessToken);
						$scope.accessToken = accessToken;						
						$scope.$apply(function () {
							$scope.accessToken = accessToken;
						});
					}
				});		
			};			

            $scope.exit = function () {
                app.exitApp();
            };

            $scope.scan = function () {
                BLE.scan();
            };

            $scope.connect = function (connectTo) {
                BLE.connect(connectTo);
            };

            $scope.disconnect = function () {
                BLE.disconnect();
                $scope.exit();
            };
}]);