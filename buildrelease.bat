cordova build --release android

cd platforms/android/build/outputs/apk

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore "D:\LarsKristian\Documents\Dropbox\AndroidKeystore\mobilars-release-key.keystore" android-release-unsigned.apk mobilars

cp android-release-unsigned.apk PersonalHub.apk
scp PersonalHub.apk larsr@tgs-fhir-web.testsenter.nhn.no:/usr/share/nginx/html/apk

cd ../../..
