﻿angular.module('fhirApp.controllers', []).controller('ObsController', ['$scope', '$q', 'BLE', 'Settings', function ($scope, $q, BLE, Settings) {

    // Init
    $scope.settings = Settings.getSettings();
    $scope.BLE = BLE;
    $scope.sensorValues = BLE.getSensorValues();
    $scope.sensorMessage = BLE.getSensorMessage();
    $scope.state = Math.round(Math.random() * 100000000).toString();

    document.addEventListener("deviceready", onDeviceReady, false);

    function onDeviceReady() {
        $scope.scan();
    }

    $scope.getPatientInfo = function () {

        $.ajax({
            type: "GET",
            url: $scope.settings.FHIR_URL + '/Patient',
            headers: {
                "Authorization": "Bearer " + $scope.accessToken
            },
            success: function (data) {
                console.log('From server:' + JSON.stringify(data));
                $scope.$apply(function () {
                    //$scope.message = 'Got patient ' + JSON.stringify(data);
                    $scope.patient = data.entry[0].resource;
                    $scope.patientId = data.entry[0].resource.id;
                    $scope.message = 'Got patient ' + $scope.patientId;
                });
            },
            datatype: 'json',
            contentType: 'application/json+fhir;charset=utf-8'
        }).fail(function (error) {
            console.log('Error while getting patient:' + error + ',' + JSON.stringify(error));
            $scope.message = 'Error while getting patient:' + error + ',' + JSON.stringify(error);
        }).done(function (data) {
            console.log("Returned success from server. Sample of data:" + JSON.stringify(data));
        });
    };

    $scope.saveHRM = function (pulse) {
        $scope.message = 'Saving data';

        var jsonPulse = {
            "resourceType": "Observation",
            "status": "final",
            "text": {
                "status": "generated",
                "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + pulse + ' bpm heart rate</div>'
            },
            "code": {
                "coding": [
                    {
                        "system": "urn:std:iso:11073:10101",
                        "code": "149530",
                        "display": "MDC_PULS_OXIM_PULS_RATE"
                    }
                ]
            },
            "valueQuantity": {
                "value": pulse,
                "unit": "bpm",
                "system": "urn:std:iso:11073:10101",
                "code": "264864"
            },
            "effectiveDateTime": new Date(),
            "reliability": "ok",
            "subject": {"reference": "Patient/" + $scope.patientId}
        };
        $scope.submitObservation(jsonPulse);
    };

    $scope.savePulseOximeterData = function (pId, Sp02, pulse) {
        $scope.message = 'Saving data';
        var jsonO2 = {
            "resourceType": "Observation",
            "text": {
                "status": "generated",
                "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, ' + pulse + ' bpm heart rate</div>'
            },
            "code": {
                "coding": [
                    {
                        "system": "urn:std:iso:11073:10101",
                        "code": "150456",
                        "display": "MDC_PULS_OXIM_SAT_O2"
                    }
                ]
            },
            "valueQuantity": {
                "value": Sp02,
                "unit": "percent",
                "system": "urn:std:iso:11073:10101",
                "code": "262688"
            },
            "effectiveDateTime": new Date(),
            "reliability": "ok",
            "subject": {"reference": "Patient/" + $scope.patientId}
        };

        var jsonPulse = {
            "resourceType": "Observation",
            "status": "final",
            "text": {
                "status": "generated",
                "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, ' + pulse + ' bpm heart rate</div>'
            },
            "code": {
                "coding": [
                    {
                        "system": "urn:std:iso:11073:10101",
                        "code": "149530",
                        "display": "MDC_PULS_OXIM_PULS_RATE"
                    }
                ]
            },
            "valueQuantity": {
                "value": pulse,
                "unit": "bpm",
                "system": "urn:std:iso:11073:10101",
                "code": "264864"
            },
            "effectiveDateTime": new Date(),
            "reliability": "ok",
            "subject": {"reference": "Patient/" + $scope.patientId}
        };
        $scope.submitObservation(jsonO2);
        $scope.submitObservation(jsonPulse);
    };

    $scope.submitObservation = function (json) {
        var accessToken = decodeURIComponent(sessionStorage.getItem('fhirAccessToken'));

        $.ajax({
            type: "POST",
            url: $scope.settings.FHIR_URL + '/Observation',
            headers: {
                "Authorization": "Bearer " + $scope.accessToken
            },
            data: JSON.stringify(json),
            success: function (data) {
                console.log('From server:' + JSON.stringify(data));
                $scope.$apply(function () {
                    $scope.message = 'Data saved to cloud. ' + JSON.stringify(data.issue[0].diagnostics);
                });
            },
            datatype: 'json',
            contentType: 'application/json+fhir;charset=utf-8'
        }).fail(function (error) {
            console.log('Save error:' + error + ',' + JSON.stringify(error));
            $scope.message = 'Save error:' + error + ',' + JSON.stringify(error);
        }).done(function (data) {
            console.log("Returned success from server. Sample of data:" + JSON.stringify(data));
        });
    };

    $scope.submitBundle = function (json) {
        $.ajax({
            type: "POST",
            url: $scope.settings.FHIR_URL + '/Bundle',
            headers: {
                "Authorization": "Bearer " + $scope.accessToken
            },
            data: JSON.stringify(json),
            success: function (data) {
                console.log('From server:' + JSON.stringify(data));
                $scope.$apply(function () {
                    $scope.message = 'Data saved to cloud';
                });
            },
            datatype: 'json',
            contentType: 'application/json+fhir;charset=utf-8'
        }).fail(function (error) {
            console.log('Save error:' + error + ',' + JSON.stringify(error));
            $scope.message = 'Save error:' + error + ',' + JSON.stringify(error);
        }).done(function (data) {
            console.log("Sample of data:" + JSON.stringify(data));
        });
    };

    $scope.saveBP = function (pId, sys, dia) {
        $scope.message = 'Saving data for patient '+pId;

        var jsonSys =
        {
            "resourceType": "Observation",
            "text": {
                "status": "generated",
                "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\"> Systolic:' + sys + '</div>'
            },
            "code": {
                "coding": [
                    {
                        "code": "150020",
                        "display": "MDC_PRESS_BLD_NONINV"
                    }
                ]
            },
            "component": [
                {
                    "code": {
                        "coding": [
                            {
                                "code": "150021",
                                "display": "MDC_PRESS_BLD_NONINV_SYS"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": sys,
                        "unit": "MDC_DIM_MMHG",
                        "code": "266016"
                    }
                },
                {
                    "code": {
                        "coding": [
                            {
                                "code": "150022",
                                "display": "MDC_PRESS_BLD_NONINV_DIA"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": dia,
                        "unit": "MDC_DIM_MMHG",
                        "code": "266016"
                    }
                }
            ],
            "effectiveDateTime": new Date(),
            "reliability": "ok",
            "subject": {"reference": "Patient/" + $scope.patientId}
        };
        $scope.submitObservation(jsonSys);
    };

    $scope.saveWeight = function (pId, weight) {
        $scope.message = 'Saving data';

        var json = {
            "resourceType": "Observation",
            "text": {
                "status": "generated",
                "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + weight + ' kg</div>'
            },
            "code": {
                "coding": [
                    {
                        "system": "urn:std:iso:11073:10101",
                        "code": "188736",
                        "display": "MDC_MASS_BODY_ACTUAL"
                    }
                ]
            },
            "effectiveDateTime": new Date(),
            "valueQuantity": {
                "value": weight,
                "unit": "kg",
                "system": "urn:std:iso:11073:10101",
                "code": "263875"
            },
            "reliability": "ok",
            "subject": {"reference": "Patient/" + $scope.patientId}
        };
        $scope.submitObservation(json);
    };

    $scope.saveSettings = function () {
        Settings.saveSettings();
    };

    $scope.loginInAppBrowserAccessToken = function () {
        var ref = cordova.InAppBrowser.open($scope.settings.authUrl, '_blank', 'location=no');
        ref.addEventListener('loadstart', function (event) {
            console.log(event.url);
            if (event.url.indexOf('access_token=') != -1) {
                ref.close();
                var accessToken = event.url.substring(event.url.indexOf('access_token=') + 13);
                accessToken = decodeURIComponent(accessToken.substr(0, accessToken.indexOf('&')));
                console.log('Received Access Token: ' + accessToken);
                sessionStorage.setItem('fhirAccessToken', accessToken);
                $scope.accessToken = accessToken;
                $scope.$apply(function () {
                    $scope.accessToken = accessToken
                    $scope.loggedIn = true;
                    $scope.message = 'Logged in';
                });
                $scope.getPatientInfo();
            }
        });
    };

    $scope.loginInAppBrowser = function () {
        var ref = cordova.InAppBrowser.open($scope.settings.authUrl, '_blank', 'location=no');
        console.log('Auth url: '+$scope.settings.authUrl);
        //var ref = cordova.InAppBrowser.open($scope.settings.authUrl, '_blank');
        ref.addEventListener('loadstart', function (event) {
            console.log(event.url);
            //if (event.url.indexOf('access_token=') != -1) {
            if (event.url.indexOf('apps.ehelselab.com') != -1 && event.url.indexOf('code=') != -1) {
                ref.close();
                var code = event.url.substring(event.url.indexOf('code=') + 5);
                console.log('Received Code: ' + code);
                code = decodeURIComponent(code.substr(0, code.indexOf('&')));
                console.log('Received Code: ' + code);
                var accessToken = $scope.getAccessTokenFromCode(code);
            }
        });
    };

    $scope.getAccessTokenFromCode = function(code) {
        console.log('Trying to get access token from Code: ' + code+' and url '+$scope.settings.TOKEN_URL);
        var formData = 'grant_type=authorization_code&code='+code+'&redirect_uri=http://apps.ehelselab.com/&username=client&password=secret&client_id=client&client_secret=secret';
        console.log('formData:'+formData);
        $.ajax({
            type: "POST",
            url: $scope.settings.TOKEN_URL,
            data: formData,
            success: function (data) {
                console.log('Success from server:' + JSON.stringify(data));
                $scope.$apply(function () {
                    $scope.message = 'Got access token:'+JSON.stringify(data);
                });

                $scope.accessToken = data.access_token;
                $scope.refreshToken = data.refresh_token;
                sessionStorage.setItem('fhirAccessToken', $scope.accessToken);
                sessionStorage.setItem('fhirRefreshToken', $scope.refreshToken);
                $scope.$apply(function () {
                    $scope.accessToken = $scope.accessToken
                    $scope.loggedIn = true;
                    $scope.message = 'Logged in';
                });
                $scope.getPatientInfo();

            },
        }).fail(function (error) {
            console.log('getAccessTokenFromCode error:' + error + ',' + JSON.stringify(error));
            $scope.message = 'getAccessTokenFromCode error:' + error + ',' + JSON.stringify(error);
        }).done(function (data) {
            console.log("Success returned from server when getting token:" + JSON.stringify(data));
        });

    };

    $scope.exit = function () {
        app.exitApp();
    };

    $scope.scan = function () {
        BLE.scan();
    };

    $scope.connect = function (connectTo) {
        BLE.connect(connectTo);
    };

    $scope.disconnect = function () {
        BLE.disconnect();
        $scope.exit();
    };
}]);
